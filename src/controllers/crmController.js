import mongoose from 'mongoose';
import {ContactSchema} from '../models/crmModel';
import {re} from "@babel/core/lib/vendor/import-meta-resolve";

const Contact = mongoose.model('Contact', ContactSchema);

export const addNewContact = (req, res) => {
    let newContact = new Contact(req.body);
    newContact.save((error, contact) => {
        if (error) {
            res.send(error);
        }
        res.json(contact);
    });
};

export const getContacts = (req, res) => {
    Contact.find({}, (error, contact) => {
        if (error) {
            res.send(error);
        }
        res.json(contact);
    });
};

export const getContactByID = (req, res) => {
    Contact.findById(req.params.contactId, (error, contact) => {
        if (error) {
            res.send(error);
        }
        res.json(contact);
    });
};

export const updateContact = (req, res) => {
    Contact.findOneAndUpdate({_id: req.params.contactId}, req.body, {returnDocument: 'after'}, (error, contact) => {
        if (error) {
            res.send(error);
        }
        res.json(contact);
    });
};

export const deleteById = (req, res) => {
    Contact.remove({_id: req.params.contactId}, (error) => {
        if (error) {
            res.send(error);
        }
        res.json({message: 'successfully delete contact'});
    });
};